from typing import Any, List,Tuple
from function_regression.functions.base_function import BaseFunction
import numpy as np
import exputils as eu

class GaussianMixtureFunction(BaseFunction):

    @staticmethod
    def default_config():
        def_config = eu.AttrDict()

        # defines how long a certain action is kept
        def_config.in_features = 2
        def_config.ranges = (-5,5)
        def_config.peak_distr_ranges = (-5,5)
        def_config.difficulty = 2
        def_config.means = np.array([[1,1],[-1,-1]])
        def_config.stds = np.array([1,1])
        def_config.coef = np.array([1,1])
        
        def_config.sample_rates = [5,5]

        return def_config


    #def __init__(self,in_features: int ,means, stds,coef = None,):
    def __init__(self, config=None, **kwargs):
   
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        if isinstance(self.config.ranges, tuple):
            self.config.ranges = [self.config.ranges] * self.config.in_features

        if isinstance(self.config.peak_distr_ranges, tuple):
            self.config.peak_distr_ranges = [self.config.peak_distr_ranges] * self.config.in_features

        print(self.config.means)
        print(self.config.stds)

        assert len(self.config.means) == len(self.config.stds), "Number of means must match the number of stds"
        assert len(self.config.means) > 0 and len(self.config.stds) > 0, "At least one gaussian has to be given"
        assert self.config.means.shape[1] == self.config.in_features, "in_features must be the same size as dim of the means"

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        input = args[0]

        value = 0

        for i in range(len(self.config.means)):
            value = value + np.exp(-0.5 * (np.linalg.norm(input - self.config.means[i]) / self.config.stds[i]) ** 2)

        return value


