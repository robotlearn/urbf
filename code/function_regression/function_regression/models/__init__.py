from .rbf_mlp import RBFMLP
from .urbf_mlp import URBFMLP
from .ffn_mlp import FFNMLP
from .svr import SVR
from .lin import Lin
from .pls import PLS
from .grad_boost_reg import GradBoostReg