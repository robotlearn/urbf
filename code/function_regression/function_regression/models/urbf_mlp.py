from typing import Any, List,Tuple
import matplotlib.pyplot as plt
import torch
import exputils as eu
import numpy as np
import torch.nn as nn
from urbf_layer.adaptive_urbf_layer import AdaptiveURBFLayer

from urbf_layer.urbf_layer import URBFLayer

class URBFMLP(torch.nn.Module):


    @staticmethod
    def default_config():
        def_config = eu.AttrDict()

        def_config.in_features = 2
        def_config.out_features = 1
        def_config.hidden_features = [16,16,8,4]
        def_config.ranges = (-5,5)
        def_config.sample_rates = 100 ### Only for plotting purposes... can be removed later
        def_config.use_urbf = True
        def_config.use_split_merge = True
        def_config.split_merge_temperature = 0.1
        def_config.dropout_rate = 0
        def_config.use_back_tray = False
        def_config.back_tray_ratio = 0.5
        def_config.grad_source='input'
        def_config.use_adaptive_range = False
        def_config.use_dynamic_architecture = False
        def_config.init_with_spektral = True

        return def_config


    def __init__(self, config=None, **kwargs):
        super().__init__()

        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        if isinstance(self.config.ranges, tuple):
            self.config.ranges = [self.config.ranges] * self.config.in_features

        if isinstance(self.config.sample_rates, int):
            self.config.sample_rates = [self.config.sample_rates] * self.config.in_features
     
        self.layers = []

        if self.config.use_urbf:
            if self.config.use_dynamic_architecture:
                self.layers.append(AdaptiveURBFLayer(in_features=self.config.in_features,out_features=self.config.hidden_features[0]))
            else:
                self.layers.append(URBFLayer(in_features=self.config.in_features,out_features=self.config.hidden_features[0],ranges=self.config.ranges,use_split_merge = self.config.use_split_merge,split_merge_temperature=self.config.split_merge_temperature, use_back_tray=self.config.use_back_tray, back_tray_ratio=self.config.back_tray_ratio,grad_signal=self.config.grad_source,use_adaptive_range=self.config.use_adaptive_range,init_with_spektral=self.config.init_with_spektral))
                self.layers.append(torch.nn.Linear(in_features=self.config.hidden_features[0],out_features=self.config.hidden_features[0]))
                self.layers.append(torch.nn.ReLU())
        else:
            N_u = self.config.hidden_features[0]
            N_in = self.config.in_features

            reduced_first_layer_size = int((2 + N_u)*N_u / (N_in + N_u))
            #reduced_first_layer_size = N_u

            self.layers.append(torch.nn.Linear(in_features=self.config.in_features,out_features=reduced_first_layer_size))
            self.layers.append(torch.nn.ReLU())
            self.layers.append(torch.nn.Linear(in_features=reduced_first_layer_size,out_features=self.config.hidden_features[0]))
            self.layers.append(torch.nn.ReLU())


        if self.config.dropout_rate > 0:
            #### we will test dropout here:
            self.layers.append(torch.nn.Dropout(p=self.config.dropout_rate))

        in_dim = self.config.hidden_features[0]
        for hidden_dim in self.config.hidden_features[1:]:
            self.layers.append(torch.nn.Linear(in_dim, hidden_dim))
            self.layers.append(torch.nn.ReLU())
            in_dim = hidden_dim
    
        self.layers.append(torch.nn.Linear(in_dim, self.config.out_features))

        self.layers = torch.nn.Sequential(*self.layers)

        self.params = nn.ModuleDict({
             'rbf': nn.ModuleList([self.layers[0].rbf_layer]) if self.config.use_urbf else nn.ModuleList([]),
             'rbf_linear': nn.ModuleList([*((self.layers[0].linear_layer,) if hasattr(self.layers[0],"linear_layer") else ())]),
             'mlp': nn.ModuleList([*self.layers[1:]]) if self.config.use_urbf else nn.ModuleList(self.layers),
            })


    def forward(self,x):
        return self.layers(x)



    ### For plotting purposes only...

    def generate_samples(self) -> Tuple:

        assert len(self.config.ranges) == self.config.in_features, "Sample Range elements must match the number of in_features"
        assert len(self.config.sample_rates) == self.config.in_features, "Sample Rate elements must match the number of in_features"

        # Generating a meshgrid for multi-dimensional sampling
        axes = [np.arange(r[0], r[1], 1/s) for r, s in zip(self.config.ranges, self.config.sample_rates)]
        meshgrid = np.meshgrid(*axes, indexing='ij')
        flat_grid = np.stack([axis.flat for axis in meshgrid], axis=-1)
        
        # Compute the values using vectorized operations
        values = np.array([self.forward(torch.from_numpy(point).to(torch.float32)).detach().numpy() for point in flat_grid])


        return np.transpose(np.array(meshgrid),(1,2,0)), values.reshape([*meshgrid[0].shape,1])

    def plot(self):

        points,values = self.generate_samples()

        assert len(points.shape) - 1 <= 3, "Can only plot functions for dim <= 3" 

        fig = plt.figure()
        ax = plt.axes(projection='3d')

        ax.contour3D(points[...,0], points[...,1], values[...,0], 50, cmap='binary')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')


