#!/usr/bin/env python
import function_regression
from repetition_config import config

# run experiment
function_regression.run_data_experiment(config=config)

